package tb.sockets.client;

import java.awt.EventQueue;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.time.LocalDateTime;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import tb.sockets.client.kontrolki.KKButton;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.border.LineBorder;

public class MainFrame extends JFrame implements ActionListener {

	private JPanel contentPane;
	Socket ServerSocket;
	static Socket sock;
    static DataInputStream dis;
    static DataOutputStream dos;
    private javax.swing.JButton ButtonSend;
    private javax.swing.JScrollPane ScrollPan;
    private static javax.swing.JTextArea msg_area;
    private javax.swing.JTextField msg_text;
    
	
	static JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
	static JFormattedTextField frmtdtxtfldXxxx2 = new JFormattedTextField();
	JLabel lblNotConnected = new JLabel("Nie po��czono"); 
	static JFormattedTextField frmtdtxtfldIp=new JFormattedTextField();
	String IP_client;
	static boolean started=true;
	
	 	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 * @throws ParseException 
	 * @throws UnknownHostException 
	 */
	public MainFrame() throws ParseException, UnknownHostException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);
		
		JFormattedTextField frmtdtxtfldIp;
		frmtdtxtfldIp = new JFormattedTextField(new MaskFormatter("###.###.#.##"));
		frmtdtxtfldIp.setBounds(43, 11, 90, 20);
		frmtdtxtfldIp.setText(IP_client);
		contentPane.add(frmtdtxtfldIp);
		
		try {
			InetAddress iAddress;
			iAddress = InetAddress.getLocalHost();
			IP_client = iAddress.getHostAddress();
					
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		JButton ButtonConnect = new JButton("Connect");
		ButtonConnect.setBounds(10, 70, 75, 23);
		contentPane.add(ButtonConnect);
		ButtonConnect.addActionListener(this);
	
		frmtdtxtfldXxxx.setText("6666");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		frmtdtxtfldXxxx2.setText("Name");
		frmtdtxtfldXxxx2.setBounds(43, 65, 90, 20);
		contentPane.add(frmtdtxtfldXxxx2);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		
		JLabel lblNick = new JLabel("Nick:");
		lblNick.setBounds(10, 68, 46, 14);
		contentPane.add(lblNick);
		

		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 138, 123, 123);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(2, 2, 5, 5));
		

		  ScrollPan = new javax.swing.JScrollPane();
	        msg_area = new javax.swing.JTextArea();
	        msg_text = new javax.swing.JTextField();
	        ButtonSend = new javax.swing.JButton();
	        msg_area.setColumns(25);
	        msg_area.setRows(10);
	        ScrollPan.setViewportView(msg_area);
	        ScrollPan.setBounds(145, 14, 487, 448);
	        contentPane.add(ScrollPan);

	        ButtonSend.setText("Send");
	        ButtonSend.setBounds(560,465, 70, 30);
	        contentPane.add(ButtonSend);
		
	        ButtonSend.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ButtonSendActionPerformed(evt);
	            }
	        });
	        msg_text.setBounds(145,465, 400, 30);
	        contentPane.add(msg_text);
	}
	private void ButtonSendActionPerformed(java.awt.event.ActionEvent evt) {
        String msgout="";
        LocalDateTime now = LocalDateTime.now();
        Integer hour = now.getHour();
        Integer minute = now.getMinute();
        Integer second = now.getSecond();
        String time=hour.toString()+":"+minute.toString()+":"+second.toString()+" ";
       try {
           msgout=msg_text.getText().trim();
           if (msg_text.getText().equals(""))
           {
           	 JOptionPane.showMessageDialog(this,"Nie mo�na wys�a� pustej wiadomo�ci!");
           }
           else{
           	dos.writeUTF(time +" Client: "+msgout);  
           	msg_area.setText(msg_area.getText().trim()+"\n"+time + msg_text.getText());
           	
           	}
           
       } catch (Exception e) {
       }
       
       msg_text.setText("");
   }
public void actionPerformed(ActionEvent event) {

	     String set="User: "+MainFrame.frmtdtxtfldXxxx2.getText();
	    setTitle(set);
		MainFrame.startChat();
					
					if(started==true)
						{
		                lblNotConnected.setText("Connected");
		                lblNotConnected.setBackground(Color.GREEN);
		                JOptionPane.showMessageDialog(this,"Connect error");
						}
					else
					{
						lblNotConnected.setText("Connect error");
		                lblNotConnected.setBackground(Color.RED);
					}
	};
static void startChat()
    {String msgin="";
        try {             	
            sock=new Socket(get_Ip(),get_Port());
            dis=new DataInputStream(sock.getInputStream());
            dos=new DataOutputStream(sock.getOutputStream());
            while(msgin.equals("exit")){
            	msgin=dis.readUTF();
                msg_area.setText(msg_area.getText().trim()+"\n"+msgin);
            }
            
        } catch (Exception e) {
        	JOptionPane.showMessageDialog(msg_area,"SERVER ERROR");
        	started=false;
        }
    }
	
static String get_Ip()
	    {
	    	String ip;
	 		if(frmtdtxtfldIp.getText().equals(""))
	     	ip="localhost";
	     	else ip=frmtdtxtfldIp.getText();
	 		return ip;
	    }
	    
static int get_Port()
	    {
	 	   int port2;
	    	
	    	if(frmtdtxtfldXxxx.getText().equals(""))port2=6666;
	    	
	    	else{
	    		String port=frmtdtxtfldXxxx.getText();
	        	port2=Integer.parseInt(port);
	    	}  
	 	return port2;
	    }
	    
		
	}

	
	    