package tb.sockets.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

import javax.swing.JOptionPane;

public class Konsola extends javax.swing.JFrame{
	public static void main(String[] args) {   
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Konsola().setVisible(true);
            }
        });
	try {
		sock = new Socket(get_IP_host(), get_Port());
		dos= new DataOutputStream(sock.getOutputStream());
		dis=new DataInputStream(sock.getInputStream());
		 while(true){
         	String msgin=dis.readUTF();
            msg_area.setText(msg_area.getText().trim()+"\n"+msgin);
         	/*so.writeChars("wysyłam tekst");
     		so.close();
     		sock.close();
     		*/
		 }
	}    catch (Exception e) {
		// TODO Auto-generated catch block
		JOptionPane.showMessageDialog(msg_area,"SERVER ERROR!");
	}
}

	private javax.swing.JButton ButtonSend;
    private javax.swing.JScrollPane ScrollPan;
    private static javax.swing.JTextArea msg_area;
    private javax.swing.JTextField msg_text;

	private void initComponents() {
		String set="User: "+MainFrame.frmtdtxtfldXxxx2.getText();
		        ScrollPan = new javax.swing.JScrollPane();
		        msg_area = new javax.swing.JTextArea();
		        msg_text = new javax.swing.JTextField();
		        ButtonSend = new javax.swing.JButton();

		        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		        setTitle(set);

		        msg_area.setColumns(20);
		        msg_area.setRows(5);
		        ScrollPan.setViewportView(msg_area);

		        ButtonSend.setText("Send");
		        ButtonSend.addActionListener(new java.awt.event.ActionListener() {
		            public void actionPerformed(java.awt.event.ActionEvent evt) {
		                ButtonSendActionPerformed(evt);
		            }
		        });

		        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		        getContentPane().setLayout(layout);
		        layout.setHorizontalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addComponent(ScrollPan)
		            .addGroup(layout.createSequentialGroup()
		                .addComponent(msg_text, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
		                .addGap(18, 18, 18)
		                .addComponent(ButtonSend, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
		        );
		        layout.setVerticalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(layout.createSequentialGroup()
		                .addComponent(ScrollPan, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
		                .addGap(18, 18, 18)
		                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
		                    .addComponent(msg_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		                    .addComponent(ButtonSend))
		                .addGap(0, 11, Short.MAX_VALUE))
		        );

		        pack();
		    }

public Konsola(){
	initComponents();
}
	static Socket sock;
	static DataInputStream dis;
	static DataOutputStream dos;
	BufferedReader reader;
	
	static String get_IP_host()
	   {
	   	String ip;
			if(MainFrame.frmtdtxtfldIp.getText().equals(""))
	    	{
	    		ip="localhost";
	    	}
	    	else{ip=MainFrame.frmtdtxtfldIp.getText();}
			return ip;
	   }
	   
	static int get_Port()
	   {
		   int port2;
	   	
	   	if(MainFrame.frmtdtxtfldXxxx.getText().equals(""))
	   	{
	   		port2=6666;
	   	}
	   	else{
	   		String port=MainFrame.frmtdtxtfldXxxx.getText();
	       	port2=Integer.parseInt(port);
	   	}  
		return port2;
	   }
	private void ButtonSendActionPerformed(java.awt.event.ActionEvent evt) {
	         String msgout="";
	         LocalDateTime now = LocalDateTime.now();
	         Integer hour = now.getHour();
	         Integer minute = now.getMinute();
	         Integer second = now.getSecond();
	         String time=hour.toString()+":"+minute.toString()+":"+second.toString()+" ";
	        try {
	            msgout=msg_text.getText().trim();
	            if (msg_text.getText().equals(""))
	            {
	            	 JOptionPane.showMessageDialog(this,"Nie mo�na wys�a� pustej wiadomo�ci!");
	            }
	            else{
	            	dos.writeUTF(time +" Client: "+msgout);  
	            	msg_area.setText(msg_area.getText().trim()+"\n"+time + msg_text.getText());
	            	
	            	}
	            
	        } catch (Exception e) {
	        }
	        
	        msg_text.setText("");
	    }
	
	
	/*void Clientready(){
		new Konsola().setVisible(true);
	
	try {             	
        sock=new Socket(get_IP_host(),get_Port());
        
        InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
        reader = new BufferedReader(streamreader);
        
        dis=new DataInputStream(sock.getInputStream());
        dos=new DataOutputStream(sock.getOutputStream());
        String msgin;
        while((msgin= reader.readLine()) != null){
        	msgin=dis.readUTF();
            msg_area.setText(msg_area.getText().trim()+"\n"+msgin);
        }
    } catch (Exception e) {
    	JOptionPane.showMessageDialog(msg_area,"NIE WYKRYTO SERWERA!");
    }
}

*/	
	
}