package tb.sockets.server;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Konsola extends javax.swing.JFrame {

public static void main(String args[]) {
		
		int  numberPort= 6666;
		String msgIN="";
		
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run(){
				new Konsola().setVisible(true);
			
			}
			});
		
	
			
		try {
			sSock = new ServerSocket(6666);
			sock = sSock.accept();//akceptacja polaczenia przez serwer
			
			dis = new DataInputStream(sock.getInputStream());
			dos = new DataOutputStream(sock.getOutputStream());
			
			 while (!msgIN.equals("exit")) {
	                msgIN = dis.readUTF();
	                msg_area.setText(msg_area.getText().trim() + "\n" + msgIN);

	            }
			/*System.out.println("Przeczytano z gniazdka: " + is.readLine());
			is.close();
			in.close();
			sock.close();
			sSock.close();
			*/
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

public Konsola(){
	initComponents();
	
}



static ServerSocket sSock;
static Socket sock;
static DataInputStream dis;
static DataOutputStream dos;

static boolean active;

private javax.swing.JScrollPane ScrollPan;
private static javax.swing.JTextArea msg_area;
private javax.swing.JTextField msg_text;
private javax.swing.JButton SendButton;
private void initComponents(){
	
	 JScrollPane ScrollPan = new javax.swing.JScrollPane();
     msg_area = new javax.swing.JTextArea();
     msg_text = new javax.swing.JTextField();
     SendButton= new javax.swing.JButton();
     
     setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
     setTitle("SerVER");
     
     ((JTextArea) msg_area).setColumns(20);
     ((JTextArea) msg_area).setRows(5);
     ScrollPan.setViewportView((Component) msg_area);
     
     msg_text.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
             msg_textActionPerformed1(evt);
         }
     });
     

     SendButton.setText("Send");
     SendButton.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
             SendButtonActionPerformed(evt);
         }
     });
     
     javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
     getContentPane().setLayout(layout);
     layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(ScrollPan)
         .addGroup(layout.createSequentialGroup()
             .addComponent(msg_text, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
             .addGap(18, 18, 18)
             .addComponent(SendButton, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
     );
     layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
             .addComponent(ScrollPan, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
             .addGap(18, 18, 18)
             .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                 .addComponent(msg_text, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                 .addComponent(SendButton))
             .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
     );

     pack();
}

private void msg_textActionPerformed1(java.awt.event.ActionEvent evt) {
    
}

private void SendButtonActionPerformed(java.awt.event.ActionEvent evt) {
    String msgout="";
    LocalDateTime now = LocalDateTime.now();
    
    
    Integer hour = now.getHour();
    Integer minute = now.getMinute();
    Integer second = now.getSecond();
    String time=hour.toString()+":"+minute.toString()+":"+second.toString()+" ";
    
	try {
        msgout=msg_text.getText().trim();
        if(msg_text.getText().equals(""))
        {
        	 JOptionPane.showMessageDialog(this,"Nie mo�na wys�a� pustej wiadomo�ci!");
        }
   
        else{ 
        	dos.writeUTF(time +" Server: "+msgout); 
        	msg_area.setText( msg_area.getText().trim()+"\n"+ time+msg_text.getText());
        	}
    } catch (Exception e) {
    }
   
    msg_text.setText("");
}
	
}


